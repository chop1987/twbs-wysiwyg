module.exports = function () {

	//install all polyfills:
	if (!Element.prototype.matches) {
		Element.prototype.matches =
			Element.prototype.matchesSelector ||
			Element.prototype.mozMatchesSelector ||
			Element.prototype.msMatchesSelector ||
			Element.prototype.oMatchesSelector ||
			Element.prototype.webkitMatchesSelector ||
			function (s) {
				var matches = (this.document || this.ownerDocument).querySelectorAll(s),
					i = matches.length;
				while (--i >= 0 && matches.item(i) !== this) { }
				return i > -1;
			};
	}

	var utils = {};

	utils.idGenerator = function () {
		var len = 8;
		var timestamp = +new Date;

		var _getRandomInt = function (min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		var ts = timestamp.toString();
		var parts = ts.split("").reverse();
		var id = "";

		for (var i = 0; i < len; ++i) {
			var index = _getRandomInt(0, parts.length - 1);
			id += parts[index];
		}

		return id;

	}

	/*
	Loops through all child elements and runs the callback function. current child element as param;
	 */
	utils.allDescendants = function (el, callback) {
		for (var i = 0; i < el.childNodes.length; i++) {
			var child = el.childNodes[i];
			utils.allDescendants(child, callback);
			callback(child);
		}

	}

	//finds first parent with certain querySelector, starting from given element
	utils.findAncestor = function (el, querySelector) {
		while ((el = el.parentElement) && !((el.matches || el.matchesSelector).call(el, querySelector)));
		return el;
	}

	utils.regexClass = function (element, pattern) {//todo: not tested
		var ar = [];
		for (var i = 0; i < element.classList.length; i++) {

			var cssClass = element.classList[i].toLowerCase();
			//if its a bs-col-*-*
			if (pattern.test(cssClass)) {
				ar.push(cssClasses[i])
			}
		}

		return ar;
	}

	utils.insertAfter = function (node, referenceNode) {
		referenceNode.parentNode.insertBefore(node, referenceNode.nextSibling);

	}

	return utils;
}