/**
 * Created by admin on 04-Dec-16.
 */
module.exports = function () {
	var self = {};

	self.modal = null;
	self.isOpen = false;


	//create modal and appendChild to given element
	self.create = function (element) {
		var modal = document.createElement('div');
		modal.classList.add('twe-modal-container');
		element.appendChild(modal);
		self.modal = modal;
		//if we want to reset modal, we can just call self.create, and isOpen needs to reset.
		self.isOpen = false;

		return modal;
	}

	self.populate = function (html) {
		if (!self.modal) {
			return;
		}
		self.modal.innerHTML = html;
		addEvents();

	}

	self.open = function () {
		if (!self.modal) return;
		self.modal.style.display = 'block';
		self.isOpen = true;
	}

	self.close = function () {
		if (!self.modal) return;
		self.modal.style.display = 'none';
		self.isOpen = false;
	}

	self.save = function () {

		if (self.onSaveCallback) {
			self.onSaveCallback();
		}
	}
	//sets a callback function to run when self.save is fired.
	self.onSave = function (callback) {
		self.onSaveCallback = callback;
	}

	self.onCancel = function (callback) {
		self.onCancelCallback = callback;
	}

	self.cancel = function () {
		if (self.onCancelCallback) {
			self.onCancelCallback();
		}
		self.close();
	}

	function addEvents() {
		var cancelB = self.modal.querySelector('.js-cancel');
		cancelB.addEventListener('click', self.cancel);

		var saveB = self.modal.querySelector('.js-save');
		saveB.addEventListener('click', self.save);
	}

	return self;
};