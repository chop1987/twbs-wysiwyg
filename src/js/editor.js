var templateEditor = require('./../templates/editor-tpl');
var templateModal = require('./../templates/modal-tpl');
var templatePropertiesContent = require('../templates/properties-content-tpl');
var templatePropertiesCol = require('../templates/properties-col-tpl');
var templatePropertiesRow = require('../templates/properties-row-tpl');
var templatePropertiesContainer = require('../templates/properties-container-tpl');

// Import TinyMCE
const tinymce = require('tinymce/tinymce');
require('tinymce/themes/modern/theme');

require('tinymce/plugins/advlist');
require('tinymce/plugins/anchor');
require('tinymce/plugins/autolink');
require('tinymce/plugins/charmap');
require('tinymce/plugins/preview');
require('tinymce/plugins/searchreplace');
require('tinymce/plugins/visualblocks');
require('tinymce/plugins/code');
require('tinymce/plugins/fullscreen');
require('tinymce/plugins/insertdatetime');
require('tinymce/plugins/media');
require('tinymce/plugins/table');
require('tinymce/plugins/contextmenu');
require('tinymce/plugins/paste');
require('tinymce/plugins/lists');
require('tinymce/plugins/link');
require('tinymce/plugins/image');
require('tinymce/plugins/print');

var tweModal = require('./twe-modal')();
var tweUtils = require('./twe-utils')();

// userCfg = {templatePath, skin_url}
module.exports = function twbEditor(userCfg = {}) {
	var self = this;
	console.log('tweutils', tweUtils);
	var config = {
		skin_url: userCfg.skin_url ? userCfg.skin_url : './skins/lightgrey',
		bsClasses: generateBsClasses(),//list of twitter bootstrap classes , used for converting those classes to custom classes and vice versa
		tweClasses: [
			'twe-container',
			'twe-container-fluid',
			'twe-row',
			'twe-col',
			'twe-section'
		]
	}

	var nodeCopy = null;

	self.init = function (elToEditor) {
		self.elToEditor = elToEditor;
		self.elToEditor.style.display = 'none';

		var container = document.createElement('div');
		container.classList.add('twe-container');

		tweUtils.insertAfter(container, self.elToEditor);

		self.container = container;
		self.container.innerHTML = templateEditor();
		self.textarea = container.querySelector('.twe-html__input');
		self.editor = container.querySelector('.twe');
		self.menu = container.querySelector('.twe-menu');
		self.absOverlay = container.querySelector('.twe__absolute');
		self.activeEl = null;
		self.contextMenu = null;
		self.minEditorHeight = 400;
		self.handleY = container.querySelector('.twe__drag-y');
		self.isResizing = false;
		self.yLast = null;

		self.originalDOM = null;
		self.editorDOM = null;

		//self.btnSave = document.querySelector('.js-save');

		//modals
		self.propsModal = null;
		addEditorEvents();
		addDragEvents();
		tweModal.create(window.document.body);
		changeGrid();

		self.container.querySelector('textarea').value = self.elToEditor.value;
		self.editorDOM = self.createEditorDom(self.elToEditor.value);
	}

	self.getValue = function () {
		var body = self.absOverlay.cloneNode(true);
		//remove .js-active class
		var jsa = body.querySelectorAll('.js-active');
		for (var i = 0; i < jsa.length; i++) {
			var element = jsa[i];
			element.classList.remove('js-active');
		}
		convertCssClasses(body, true);
		return body.innerHTML;
	}

	function generateBsClasses() {
		var bsAr = [
			'section',
			'container',
			'container-fluid',
			'row'
		];

		for (var i = 1; i < 13; i++) {
			var ar = [
				'col-xs-' + i,
				'offset-xs-' + i,
				'col-sm-' + i,
				'offset-sm-' + i,
				'col-md-' + i,
				'offset-md-' + i,
				'col-lg-' + i,
				'offset-lg-' + i,
				'col-xl-' + i,
				'offset-xl-' + i,
			];
			bsAr = bsAr.concat(ar);
		}
		return bsAr;
	}

	function stringToDom(htmlString) {
		var parser = new DOMParser();
		var doc = parser.parseFromString(htmlString, "text/html");
		return doc;
	}

	function removeActiveClass() {
		self.absOverlay.classList.remove('js-active')
		var els = self.absOverlay.querySelectorAll('.js-active');
		for (var i = 0; i < els.length; i++) {
			els[i].classList.remove('js-active');
		}
	}

	function addEditorEvents() {
		var btnAddSection = self.menu.querySelector('.js-add-section');
		btnAddSection.addEventListener('click', addSection);

		var btnAddContainer = self.menu.querySelector('.js-add-container');
		btnAddContainer.addEventListener('click', addContainer);

		var btnAddRow = self.menu.querySelector('.js-add-row');
		btnAddRow.addEventListener('click', addRow);

		var btnAddCol = self.menu.querySelector('.js-add-col');
		btnAddCol.addEventListener('click', addCol);

		var btnAddContent = self.menu.querySelector('.js-add-content');
		btnAddContent.addEventListener('click', addContent);

		var btnDeleteNode = self.menu.querySelector('.js-delete-node');
		btnDeleteNode.addEventListener('click', deleteNode);

		var selectGridSize = self.menu.querySelector('#grid-size');
		selectGridSize.addEventListener('change', changeGrid);

		//events in the editor
		self.absOverlay.addEventListener('click', function (e) {
			e.stopPropagation();
			removeActiveClass();
			self.activeEl = self.absOverlay;
		});
		self.absOverlay.addEventListener('click', hideContextMenu);

		self.absOverlay.addEventListener('click', function (e) {
			['twe-row', 'twe-col', 'twe-container', 'twe-content', 'twe-section'].forEach(function (elClass) {
				if (e.srcElement.classList.contains(elClass)) {
					el = e.target;
					// var target = e.target || e.srcElement
					e.stopPropagation();
					removeActiveClass();
					self.activeEl = el;
					el.classList.add('js-active');
					hideContextMenu();
				};
			})
		})
		self.absOverlay.addEventListener('contextmenu', openContextMenu);

		//event for textarea
		document.querySelector('.twe-html__input').addEventListener("input", function () {
			self.createEditorDom();
		});
	}

	function addSection(e) {
		e.preventDefault();

		if (self.activeEl === null) {
			console.log('no active selected');
			self.activeEl = self.absOverlay;
		}
		var section = document.createElement('section');
		section.classList.add('twe-section');
		section.classList.add('bs-section');
		addElData(section);

		self.activeEl.appendChild(section);
		saveToInput();
	}

	function addContainer(e) {
		e.preventDefault();

		if (self.activeEl === null) {
			console.log('no active selected');
			self.activeEl = self.absOverlay;
		}

		var container = document.createElement('div');
		container.classList.add('twe-container');
		container.classList.add('bs-container');
		addElData(container);

		self.activeEl.appendChild(container);
		saveToInput();
	}

	function addRow(e) {
		e.preventDefault();

		if (self.activeEl === null) {
			console.log('no active selected');
			self.activeEl = self.absOverlay;
		}

		var row = document.createElement('div');
		row.classList.add('twe-row');
		row.classList.add('bs-row');
		addElData(row);

		self.activeEl.appendChild(row);
		saveToInput();
	}

	function addCol(e) {
		e.preventDefault();

		if (self.activeEl === null) {
			console.log('no active selected');
			self.activeEl = self.absOverlay;
		}

		var col = document.createElement('div');
		col.classList.add('twe-col');
		col.classList.add('bs-col-xs-12');
		addElData(col);

		self.activeEl.appendChild(col);
		saveToInput();
	}

	function addContent(e) {
		e.preventDefault();

		if (self.activeEl === null) {
			console.log('no active selected');
			self.activeEl = self.absOverlay;
		}

		var content = document.createElement('div');
		content.classList.add('twe-content');
		//addElData(content);
		self.activeEl.appendChild(content);
		saveToInput();
	}

	function copyNode(deleteNode) {//if deleteNode then it means CUT
		nodeCopy = self.activeEl.cloneNode(true);

		if (deleteNode == true) {//cut
			self.activeEl.parentNode.removeChild(self.activeEl);
			saveToInput();
		} else {//copy

			//when copying, we need a new data-el-id because it must be identical
			var uId = tweUtils.idGenerator();
			nodeCopy.dataset.elId = uId;

			//also do this for all children
			tweUtils.allDescendants(nodeCopy, function (el) {
				for (var i = 0; i < config.tweClasses.length; i++) {
					if (el.tagName && el.classList.contains(config.tweClasses[i])) {
						var uId = tweUtils.idGenerator();
						el.dataset.elId = uId;
						continue;
					}
				}
			})
		}
		hideContextMenu();
	}

	function pasteNode() {
		if (!nodeCopy) {
			return
		}
		//if its twe__absolute, we cannot paste nodeCopy in directly.
		//First serach for body first and append it in there
		if (self.activeEl.classList.contains('twe__absolute')) {
			self.activeEl.appendChild(nodeCopy);

			//resset nodeCOpy
			nodeCopy = null;
			saveToInput();
		} else {
			self.activeEl.appendChild(nodeCopy);
			//resset nodeCOpy
			nodeCopy = null;
			saveToInput();
		}

		hideContextMenu();
	}

	function addElData(el) {
		//create elId to identify that this is a editable element in editor;
		var uId = tweUtils.idGenerator();
		el.dataset.elId = uId;
	}

	/*
	@param domString (string) : html markup as string
	@return DOM
	 */
	self.createEditorDom = function (htmlString) {
		var strng = htmlString || self.textarea.value;
		self.absOverlay.innerHTML = "";
		newDOM = stringToDom(strng).cloneNode(true);
		convertCssClasses(newDOM);
		//popuplate editor with the editorDOM
		for (var i = 0; i < newDOM.body.childNodes.length; i++) {
			var node = newDOM.body.childNodes[i].cloneNode(true);
			self.absOverlay.appendChild(node);
		}

		return;
	}

	/*
	 @param el (DOM element) : element which css classes need to be converted
	 @param toBs (bool) : if true, remove all twe- prefixes (plain html)
	 */
	function convertCssClasses(el, toBS) {
		if (toBS) {
			tweUtils.allDescendants(el, function () {
				//check for all elements with a BS class
				for (var i = 0; i < config.bsClasses.length; i++) {
					var twClass = 'bs-' + config.bsClasses[i];
					var els = el.querySelectorAll('.' + twClass);
					for (var y = 0; y < els.length; y++) {
						els[y].classList.remove('js-active');
						//if element doesnt have data-el-id, don't convert classes
						if (!els[y].dataset.elId) continue;
						els[y].classList.remove(twClass);
						els[y].classList.add(config.bsClasses[i]);
					}
				}
			})
		} else {//to twbs wysiwyg compatible html
			//check for all elements with a BS class
			console.log('pls let section work');
			for (var i = 0; i < config.bsClasses.length; i++) {
				var bsClass = config.bsClasses[i];
				var els;
				if (bsClass === 'section') {
					els = el.querySelectorAll('section');
				} else {
					els = el.querySelectorAll('.' + bsClass);
				}
				for (var y = 0; y < els.length; y++) {
					if (!els[y].dataset.elId) continue;
					els[y].classList.remove(bsClass);
					els[y].classList.add('bs-' + bsClass);
				}
			}
		}
	}

	function deleteNode() {
		if (self.activeEl === self.absOverlay) {
			alert('nothing to delete selcted');
			return;
		}
		var el = self.activeEl;
		var check = confirm('Are you sure you want to delete?');
		if (check) {
			try {
				el.parentNode.removeChild(el);
				saveToInput();
				hideContextMenu();
			} catch (error) {
				console.log(error);
			}
		}
	}

	function changeGrid(e) {
		//if this is not called from eventListener (but in method init);
		if (!e) {
			self.absOverlay.classList.add('grid-xs');
			self.absOverlay.classList.remove('grid-sm');
			self.absOverlay.classList.remove('grid-md');
			self.absOverlay.classList.remove('grid-lg');
			self.absOverlay.classList.remove('grid-xl');
			return;
		}

		switch (e.target.options[e.target.selectedIndex].value) {
			case 'xs':
				self.absOverlay.classList.add('grid-xs');
				self.absOverlay.classList.remove('grid-sm');
				self.absOverlay.classList.remove('grid-md');
				self.absOverlay.classList.remove('grid-lg');
				self.absOverlay.classList.remove('grid-xl');
				break;
			case 'sm':
				self.absOverlay.classList.add('grid-xs');
				self.absOverlay.classList.add('grid-sm');
				self.absOverlay.classList.remove('grid-md');
				self.absOverlay.classList.remove('grid-lg');
				self.absOverlay.classList.remove('grid-xl');
				break;
			case 'md':
				self.absOverlay.classList.add('grid-xs');
				self.absOverlay.classList.add('grid-sm');
				self.absOverlay.classList.add('grid-md');
				self.absOverlay.classList.remove('grid-lg');
				self.absOverlay.classList.remove('grid-xl');
				break;
			case 'lg':
				self.absOverlay.classList.add('grid-xs');
				self.absOverlay.classList.add('grid-sm');
				self.absOverlay.classList.add('grid-md');
				self.absOverlay.classList.add('grid-lg');
				self.absOverlay.classList.remove('grid-xl');
				break;
			case 'xl':
				self.absOverlay.classList.add('grid-xs');
				self.absOverlay.classList.add('grid-sm');
				self.absOverlay.classList.add('grid-md');
				self.absOverlay.classList.add('grid-lg');
				self.absOverlay.classList.add('grid-xl');
				break;
			default:
				console.log('something went wrong changing grid');
				break;
		}
	}

	function calcMinEditorHeight() {
		var rows = self.absOverlay.querySelectorAll('.twe-row');
		if (rows.length < 1) {
			return;
		}
		var totalHeight = 0;
		for (var i = 0; i < rows.length; i++) {
			var row = rows[i];
			var h = window.getComputedStyle(row, null).getPropertyValue('height');
			var mt = window.getComputedStyle(row, null).getPropertyValue('margin-top');
			var mb = window.getComputedStyle(row, null).getPropertyValue('margin-bottom');

			var height = h.match(/\d+/);
			var marginTop = mt.match(/\d+/);
			var marginBottom = mb.match(/\d+/);
			totalHeight += parseInt(height) + parseInt(marginTop) + parseInt(marginBottom);
		}
		//the 50 is a "margin" just in case of errors..
		return totalHeight + 50;
		//get elements .getBoundingClientRect()
	}

	function addDragEvents() {
		self.handleY.addEventListener('mousedown', function (e) {
			self.isResizing = true;
			//calculate and remember the total height of the absolute elements.
			//later on needed to make sure you cannot make the view smaller;
			self.minEditorHeight = calcMinEditorHeight();

			self.yLast = e.screenY;
		});

		self.handleY.addEventListener('mouseup', function (e) {
			self.isResizing = false;
		});
		//adding the event on global mouseup event because there is a chance the mouse is noit going to be on the handleY through
		self.handleY.addEventListener('mousemove', mouseMove);

		self.handleY.addEventListener('mouseout', function () {
			self.isResizing = false;
			//console.log('stop resizing in mouseout');
		})
		//todo: find better way to fix the drag bug
		self.editor.oncontextmenu = function (e) {
			e.stopPropagation();
			if (self.isResizing) {
				self.isResizing = false;
				return false;
			}
		}
	}

	function mouseMove(e) {

		// we don't want to do anything if we aren't resizing.
		if (!self.isResizing) {
			return;
		}

		if (self.yLast != null) {
			var y = self.yLast - e.screenY;
			self.editor.style.height = parseInt(self.editor.offsetHeight) - y + 'px';
			self.yLast = e.screenY;
		}
	}

	function openContextMenu(e) {
		e.stopPropagation();
		e.preventDefault();

		//do not open context menu if it is trigger by one of the classes in ignore[];
		var ignore = ['.twe-content *'];
		var tweContentChild = false;
		for (var i = 0; i < ignore.length; i++) {

			/*
			 if  e.target matches twe-content * it's a child of twe-content
			 */
			if (e.target.matches(ignore[i])) {
				tweContentChild = true;
			}
		}

		removeActiveClass();

		if (tweContentChild) {
			//parent twe-content add class js-active
			// self.activeEl = parent twe-content
			var tweContent = tweUtils.findAncestor(e.target, '.twe-content');
			self.activeEl = tweContent;
			tweContent.classList.add('js-active');
		} else {
			self.activeEl = e.target;
			e.target.classList.add('js-active');
		}

		buildContextMenu(e);
		return false;
	}

	function buildContextMenu(e) {
		if (self.contextMenu) {
			hideContextMenu();
		}

		var cm = document.createElement('div');
		cm.classList.add('tw-cm');

		ul = document.createElement('ul');
		ul.classList.add('tw-cm__list');

		//if twe__absolute
		if (self.activeEl.classList.contains('twe__absolute') && nodeCopy) {
			var liPaste = createCMLi('Paste Node', pasteNode);
			ul.appendChild(liPaste);
		} else if (self.activeEl.classList.contains('twe-section')) {
			var liMoveUp = createCMLi('Move Up', function () {
				moveEl('up');
			});
			ul.appendChild(liMoveUp);

			var liMoveDown = createCMLi('Move Down', function () {
				moveEl('down')
			});
			ul.appendChild(liMoveDown);

			var liCut = createCMLi('CutNode', function () { copyNode(true) });
			ul.appendChild(liCut);

			var liCopy = createCMLi('Copy Node', copyNode);
			ul.appendChild(liCopy);

			var liPaste = createCMLi('Paste Node', pasteNode);
			ul.appendChild(liPaste);

			var liDelete = createCMLi('delete Node', deleteNode);
			ul.appendChild(liDelete);

			var liProperties = createCMLi('Properties', function () {
				showProperties('section');
			});
			ul.appendChild(liProperties);
		}
		//if CONTAINER / CONTAINER FLUID
		else if (self.activeEl.classList.contains('twe-container')) {
			var liMoveUp = createCMLi('Move Up', function () {
				moveEl('up');
			});
			ul.appendChild(liMoveUp);

			var liMoveDown = createCMLi('Move Down', function () {
				moveEl('down')
			});
			ul.appendChild(liMoveDown);

			var liCut = createCMLi('CutNode', function () { copyNode(true) });
			ul.appendChild(liCut);

			var liCopy = createCMLi('Copy Node', copyNode);
			ul.appendChild(liCopy);

			var liPaste = createCMLi('Paste Node', pasteNode);
			ul.appendChild(liPaste);

			var liDelete = createCMLi('delete Node', deleteNode);
			ul.appendChild(liDelete);

			var liProperties = createCMLi('Properties', function () {
				showProperties('container');
			});
			ul.appendChild(liProperties);
		}
		//if COLUMN
		else if (self.activeEl.classList.contains('twe-col')) {

			var liMoveUp = createCMLi('Move Up', function () {
				moveEl('up');
			});
			ul.appendChild(liMoveUp);

			var liMoveDown = createCMLi('Move Down', function () {
				moveEl('down')
			});
			ul.appendChild(liMoveDown);

			var liCut = createCMLi('CutNode', function () { copyNode(true) });
			ul.appendChild(liCut);

			var liCopy = createCMLi('Copy Node', copyNode);
			ul.appendChild(liCopy);

			var liPaste = createCMLi('Paste Node', pasteNode);
			ul.appendChild(liPaste);

			var liDelete = createCMLi('delete Node', deleteNode);
			ul.appendChild(liDelete);

			var liProperties = createCMLi('Properties', function () {
				showProperties('col');
			});
			ul.appendChild(liProperties);
		}
		//IF ROW
		else if (self.activeEl.classList.contains('twe-row')) {
			var liMoveUp = createCMLi('Move Up', function () {
				moveEl('up');
			});
			ul.appendChild(liMoveUp);

			var liMoveDown = createCMLi('Move Down', function () {
				moveEl('down')
			});
			ul.appendChild(liMoveDown);

			var liCut = createCMLi('CutNode', function () { copyNode(true) });
			ul.appendChild(liCut);

			var liCopy = createCMLi('Copy Node', copyNode);
			ul.appendChild(liCopy);

			var liPaste = createCMLi('Paste Node', pasteNode);
			ul.appendChild(liPaste);

			var liDelete = createCMLi('delete Node', deleteNode);
			ul.appendChild(liDelete);

			var liProperties = createCMLi('Properties', function () {
				showProperties('row');
			});
			ul.appendChild(liProperties);
		}
		//IF is TinyMCE
		else if (self.activeEl.classList.contains('twe-content')) {
			var liMoveUp = createCMLi('Move Up', function () {
				moveEl('up');
			});
			ul.appendChild(liMoveUp);

			var liMoveDown = createCMLi('Move Down', function () {
				moveEl('down')
			});
			ul.appendChild(liMoveDown);

			var liCut = createCMLi('Cut Node', function () { copyNode(true) });
			ul.appendChild(liCut);

			var liCopy = createCMLi('Copy Node', copyNode);
			ul.appendChild(liCopy);

			var liPaste = createCMLi('Paste Node', pasteNode);
			ul.appendChild(liPaste);

			var liDelete = createCMLi('delete Node', deleteNode);
			ul.appendChild(liDelete);

			var liProperties = createCMLi('Edit', function () {
				showProperties('twe-content');
			});
			ul.appendChild(liProperties);
		}

		cm.appendChild(ul);
		self.container.appendChild(cm);
		self.contextMenu = cm;
		var wh = window.innerHeight;
		var ww = window.innerWidth;
		var x = e.clientX;
		var y = e.clientY;

		self.contextMenu.style.display = "block";

		if (x < (ww / 2)) {//if clicked on left side of the screen
			self.contextMenu.style.left = x + 'px';
			self.contextMenu.style.top = y + 'px';
		} else { //if clicked right side of the screen
			self.contextMenu.style.left = (x - cm.clientWidth) + 'px';
			self.contextMenu.style.top = y + 'px';
		}
	}

	function hideContextMenu() {
		if (self.contextMenu && self.contextMenu.parentNode) {
			self.contextMenu.parentNode.removeChild(self.contextMenu);
		}
	};

	function createCMLi(title, fnc) {
		var li = document.createElement('li');
		li.classList.add('tw-cm__list__item');
		var a = document.createElement('a');
		a.href = "#";
		a.text = title;
		a.classList.add('tw-cm__list__item__link');
		a.addEventListener('click', fnc);
		li.appendChild(a);
		return li;
	}

	function moveEl(direction) {
		var parent = self.activeEl.parentNode;
		switch (direction) {
			case 'up':
				var prev = self.activeEl.previousSibling;

				if (prev) {
					parent.insertBefore(self.activeEl, prev);
					saveToInput();
				}
				break;
			case 'down':
				if (self.activeEl.nextSibling && self.activeEl.nextElementSibling.nextElementSibling) {
					parent.insertBefore(self.activeEl, self.activeEl.nextElementSibling.nextElementSibling);
					saveToInput();
				} else if (self.activeEl.nextSibling && !self.activeEl.nextElementSibling.nextElementSibling) {
					parent.appendChild(self.activeEl);
					saveToInput();
				}
				break;
			default:
				break;
		}
	}

	/**
	 * Opens the modal with the right template and data
	 * @param {String} propType 
	 */
	function showProperties(propType) {
		var cssClasses = self.activeEl.classList;
		var patternColSize = new RegExp('^bs-col-[A-Za-z]{2}-[0-9]{1,2}$');
		var patternColOffset = new RegExp('^bs-offset-[A-Za-z]{2}-[0-9]{1,2}$');
		var patternRow = new RegExp('(^bs-row)|(^twe-row)|(^js-active)$');
		var patternContainer = new RegExp('(^bs-container$)|(^bs-container-fluid$)');
		if (propType == 'col') {
			var backupClasses = [];
			//map classes of element
			var elData = {};
			//loop through all css classes that the element has
			for (var i = cssClasses.length - 1; i > 0; i--) {

				var cssClass = cssClasses[i].toLowerCase();
				//if its a bs-col-*-*
				if (patternColSize.test(cssClass)) {
					var size = cssClass.split("-")[2];
					var num = cssClass.split("-")[3];
					elData[size] = num;
					self.activeEl.classList.remove(cssClass);
				}
				//if its a bs-offset-*-*
				if (patternColOffset.test(cssClass)) {
					var size = cssClass.split("-")[2];
					var num = cssClass.split("-")[3];
					elData[size + 'o'] = num;
					self.activeEl.classList.remove(cssClass);
				}
				backupClasses.push(cssClass);
			}

			hideContextMenu();
			tweModal.populate(templateModal({
				partial: templatePropertiesCol,
				data: elData
			}));
			tweModal.onSave(function () {
				var form = tweModal.modal.querySelector('form');
				//add back classes if defined;
				form.cxs.value && self.activeEl.classList.add('bs-col-xs-' + form.cxs.value);
				form.csm.value && self.activeEl.classList.add('bs-col-sm-' + form.csm.value);
				form.cmd.value && self.activeEl.classList.add('bs-col-md-' + form.cmd.value);
				form.clg.value && self.activeEl.classList.add('bs-col-lg-' + form.clg.value);
				form.cxl.value && self.activeEl.classList.add('bs-col-xl-' + form.cxl.value);

				form.cxso.value && self.activeEl.classList.add('bs-offset-xs-' + form.cxso.value);
				form.csmo.value && self.activeEl.classList.add('bs-offset-sm-' + form.csmo.value);
				form.cmdo.value && self.activeEl.classList.add('bs-offset-md-' + form.cmdo.value);
				form.clgo.value && self.activeEl.classList.add('bs-offset-lg-' + form.clgo.value);
				form.cxlo.value && self.activeEl.classList.add('bs-offset-xl-' + form.cxlo.value);

				saveToInput();
				tweModal.close();
			});
			tweModal.onCancel(function () {
				for (var i = 0; i < backupClasses.length; i++) {
					self.activeEl.classList.add(backupClasses[i]);
				}
			})
			tweModal.open();

		} else if (propType == 'row') {
			//cssClasses
			//map classes of element
			var elData = {};
			var backupClasses = [];
			elData.cssClasses = "";
			//loop through all css classes that the element has
			for (var i = cssClasses.length - 1; i > 0; i--) {
				var cssClass = cssClasses[i];
				backupClasses.push(cssClass);
				//if its not a bs-col-*-* add to the custom csss list and remove
				if (!patternRow.test(cssClass)) {
					elData.cssClasses += " " + cssClass;
					self.activeEl.classList.remove(cssClass);
				}
			}

			hideContextMenu();
			tweModal.populate(templateModal({
				partial: templatePropertiesRow,
				data: elData
			}));
			tweModal.onSave(function () {
				var form = tweModal.modal.querySelector('form');
				console.log(form.cssClasses.value);
				//todo create json and set in data attirbute
				var newClasses = form.cssClasses.value.split(" ");
				for (var i = 0; i < newClasses.length; i++) {
					if (newClasses[i] !== "") {
						self.activeEl.classList.add(newClasses[i]);
					}
				}
				saveToInput();
				tweModal.close();
			});
			tweModal.onCancel(function () {
				for (var i = 0; i < backupClasses.length; i++) {
					self.activeEl.classList.add(backupClasses[i]);
				}
			})
			tweModal.open();

		} else if (propType === 'container') {
			//map classes of element
			var elData = {};
			var backupClasses = [];
			elData.cssClasses = "";
			//loop through all css classes that the element has
			for (var i = cssClasses.length - 1; i > 0; i--) {

				var cssClass = cssClasses[i];
				backupClasses.push(cssClass);
				//if its not a bs-container-* add to the custom csss list and remove
				if (!patternContainer.test(cssClass)) {
					elData.cssClasses += " " + cssClass;
					self.activeEl.classList.remove(cssClass);
				} else {//if is bs-container(-fluid)
					elData.containerType = cssClass;
				}
			}

			hideContextMenu();
			tweModal.populate(templateModal({
				partial: templatePropertiesContainer,
				data: elData
			}))

			tweModal.onSave(function () {
				var form = tweModal.modal.querySelector('form');
				console.log(form);
				//todo create json and set in data attirbute
				var newClasses = form.cssClasses.value.split(" ");
				for (var i = 0; i < newClasses.length; i++) {
					if (newClasses[i] !== "") {
						self.activeEl.classList.add(newClasses[i]);
					}
				}
				//change the bs-container /fluid class
				if (form.containerType.value === 'container-fluid') {
					self.activeEl.classList.remove('bs-container');
					self.activeEl.classList.add('bs-container-fluid');
				} else {
					self.activeEl.classList.remove('bs-container-fluid');
					self.activeEl.classList.add('bs-container');
				}
				saveToInput();
				tweModal.close();
			});
			tweModal.onCancel(function () {
				for (var i = 0; i < backupClasses.length; i++) {
					self.activeEl.classList.add(backupClasses[i]);
				}
			})
			tweModal.open();

		} else if (propType == 'twe-content') {
			//hideContextMenu();
			//openTinyMCE();
			var backupHTML = self.activeEl.innerHTML;

			hideContextMenu();
			// tweModal.populate(modalTemplate);
			tweModal.populate(templateModal({
				partial: templatePropertiesContent,
				data: self.activeEl.innerHTML
			}));
			var editor = openTinyMCE('#editorcontent');
			tweModal.onSave(function () {

				//get value of textarea and overwrite self.activeEL.innerHTML with it
				self.activeEl.innerHTML = tinyMCE.activeEditor.getContent();
				//save to the invisible Input
				saveToInput();

				tweModal.close();
				//remove editor so it can reinit again when needed
				tinymce.EditorManager.execCommand('mceRemoveEditor', true, tinymce.get()[0].id);
			});
			tweModal.onCancel(function () {
				//self.activeEl.innerHTML = backupHTML;

				//remove editor so it can reinit again when needed
				tinymce.EditorManager.execCommand('mceRemoveEditor', true, tinymce.get()[0].id);
			})
			tweModal.open();
		}

	}//END showProperties()

	//saving to editor input, to editor textarea. NOT MISTAKEN with textarea which the editor is bound on.
	function saveToInput() {
		self.textarea.value = self.absOverlay.innerHTML;
	}

	//used before saving html
	//todo: not tested!
	function stripClasses(dom) {
		classesAr = [
			'js-active'
		];

		for (var i = 0; i < classesAr.length; i++) {
			var els = dom.querySelectorAll(classesAr[i]);
			for (var x = 0; x < els.length; x++) {
				els[x].classList.remove(classesAr[i]);
			}
		}
	}

	function openTinyMCE(selector) {
		var el = tweUtils.findAncestor(self.activeEl, '[data-el-id]');
		var id = el.dataset.elId;

		tinymce.init({
			//selector: '[data-el-id="' + id + '"] .twe-content',
			selector: selector,
			menubar: false,
			skin_url: config.skin_url,
			plugins: [
				'advlist autolink lists link image charmap print preview anchor',
				'searchreplace visualblocks code fullscreen',
				'insertdatetime media table contextmenu paste code'
			],
			toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
			content_css: '//www.tinymce.com/css/codepen.min.css'
		});
	}
	return this;
}