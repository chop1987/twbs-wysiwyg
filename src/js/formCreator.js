/*
form		:	object {title: 'form 1', method: 'post', name: 'formBla', fields: objArrayFields}
form.fields	=	objArrayFields	= [{label : 'first name', name : 'fname', description : 'bladibla bla', type : 'text', value : 'derp'}]
 */

var formCreator = (function(){
	var self = {};
	self.create = function (formObj){

		console.log(this);
		var title = formObj.title;
		var method = formObj.method || null;
		var name = formObj.name || null;
		var fields = formObj.fields;
		var form = document.createElement('form');

		//create form
		form.classList.add('tbwe-form');

		for(var i = 0; i < fields.length; i++){

			var row = createFormRow(fields[i]);
			form.appendChild(row);


		}

		//todo: delete this after test
		
		return this;
	}

	function createFormRow(fieldObj) {
		var row = document.createElement('div');
		row.classList.add('twbe-form__row');

		var input, label;

		switch(fieldObj.type){
			case 'text':
				/*exampe object:
				 {
				 label : 'first name',
				 name : 'fname',
				 description : 'bladibla bla',
				 type : 'inputText',
				 value : 'derp'
				 }

				 */

				input = document.createElement('input');
				input.type = 'text';
				input.value = fieldObj.value || '';

				label = document.createElement('label');
				label.classList.add('twbe-form__row__label');
				label.innerHTML = fieldObj.label;
				label.appendChild(input);

				if(fieldObj.description){
					var desc = document.createElement('p');
					desc.innerHTML = fieldObj.description;
					row.appendChild(desc);
				}
				row.appendChild(label);

				return row;
				break;
			case 'inputRadio':
				/*
				 name (should be same),
				 checked (attribute with no value),
				 value
				 */
				break;
			case 'inputCheckbox':

				break;
			case 'inputColor':

				break;
			case 'inputNumber':
				/*
				 min,
				 max,
				 step,
				 value
				 */
				break;
			case 'inputUrl':

				break;
			case 'inputRange':

				break;
			case 'inputEmail':

				break;
			case 'inputSearch':

				break;
			case 'select':
				/*
				 {
				 type: 'select'
				 label: 'select a car',
				 name: 'carselect',
				 description: 'what care do you have?'
				 multiple: true / false (optional),
				 required: true / false (optional),
				 size: int (optional),
				 options: [
				 {
				 text: 'Volvo',
				 value: 'volvo'
				 },
				 {
				 text: 'Audi',
				 value: 'audi'
				 },
				 ]

				 }
				 */
				var select = document.createElement('select');
				select.name = fieldObj.name || '';
				if(fieldObj.size){ select.size = fieldObj.size };

				for (var i = 0; i < fieldObj.options.length; i++) {
					var option = document.createElement("option");
					option.value = fieldObj.options[i].value;
					option.text = fieldObj.options[i].text;
					select.appendChild(option);
				}

				label = document.createElement('label');
				label.classList.add('twbe-form__row__label');
				label.innerHTML = fieldObj.label;
				label.appendChild(select);

				row.appendChild(label);

				if(fieldObj.description){
					var desc = document.createElement('p');
					desc.innerHTML = fieldObj.description;
					row.appendChild(desc);
				}

				return row;

				break;
			default:
				console.log('no valid input type detected');
				break;

		}



	}

	return self;
})()