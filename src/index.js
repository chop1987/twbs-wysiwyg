require('./scss/start.scss');

import twbEditor from './js/editor';

// example how to use:
// var templatePath = '../dist/templates';
// var skin_url = '../dist/skins/lightgray';
// var dummyHTML = '<section data-el-id="13187000" class="twe-section vbyby"><div class="twe-row herp derp smerp row" data-el-id="13187491"><div class="twe-col col-xs-offset-1 col-md-offset-2 col-xl-6 col-xs-10 col-md-10" data-el-id="12113164"><div class="twe-content js-active"><div class="row"><p>Hello you</p> </div></div></div></div><div class="twe-row herp derp smerp row" data-el-id="47773477"><div class="twe-col col-xs-offset-1 col-md-offset-2 col-xl-6 col-xs-10 col-md-10" data-el-id="64734174"><div class="twe-content">Hello you</div></div></div></section>'
// var c = document.querySelector('textarea'); //is a textarea

// c.value = dummyHTML;

// var t = new twbEditor({ templatePath, skin_url });
// t.init(c);
// console.log(t);

module.exports = require('./js/editor');

