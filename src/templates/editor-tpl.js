module.exports = () => `
<div class="twe-html">
    <textarea class="twe-html__input"></textarea>
</div>
<div class="twe-menu">
    <button class="twe-btn twe-btn--default js-add-section"><span>+ section</span></button>
    <button class="twe-btn twe-btn--default js-add-container"><span>+ container</span></button>
    <button class="twe-btn twe-btn--default js-add-row"><span>+ row</span></button>
    <button class="twe-btn twe-btn--default js-add-col"><span>+ column</span></button>
    <button class="twe-btn twe-btn--default js-add-content"><span>+ Content</span></button> |
    <button class="twe-btn twe-btn--red js-delete-node"><span>delete</span></button>
    <select name="" id="grid-size" class="twe-menu__select">
        <option value="xs">XS</option>
        <option value="sm">SM</option>
        <option value="md">MD</option>
        <option value="lg">LG</option>
        <option value="xl">XL</option>
    </select>
</div>
<div class="twe">
    <div class="twe__absolute">

    </div>
    <div class="twe__bg">
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>
        <div class="twe__bg__col"></div>

    </div>
    <div class="twe__drag-y"></div>
</div>
`;