module.exports = ({ data }) => {
	return `<form action="" class="twe-form">
		<div class="twe-form__row">
			<input type="text" name="cssClasses" id="cssClasses" required placeholder="css classes" value="${ data.cssClasses}">
			<label for="cssClasses">
				Css classes
			</label>
		</div>
	</form>`;
};