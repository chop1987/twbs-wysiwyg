module.exports = ({ data }) => {
	const containerSelected = data.containerType === 'bs-container' ? 'selected' : '';
	const containerFluidSelected = data.containerType === 'bs-container-fluid' ? 'selected' : '';
	return `<form action="" class="twe-form">
				<div class="twe-form__row">
					<input type="text" name="cssClasses" id="cssClasses" required placeholder="css classes" value="${data.cssClasses}">
					<label for="cssClasses">
						Css classes
					</label>
				</div>
				<div class="twe-form__row">
					<select type="text" name="containerType" id="containerType" required placeholder="container type">
						<option 
							value="container" 
							${containerSelected}>
								container
						</option>
						<option 
							value="container-fluid" 
							${containerFluidSelected}>
								container-fluid
						</option>
					</select>
					<span>container type</span>
				</div>
			</form>`;
}