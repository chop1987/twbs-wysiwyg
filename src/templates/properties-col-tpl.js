module.exports = ({ data }) => {
	const defaultData = {
		xs: '',
		sm: '',
		md: '',
		lg: '',
		xl: '',
		xso: '',
		smo: '',
		mdo: '',
		lgo: '',
		xlo: '',
	};

	const combined = { ...defaultData, ...data };
	console.log('properties-col the real data:', combined);
	return `<form action="" class="twe-form">
		<div class="twe-form twe-form__col--6">
			<div class="twe-form__row">
				<input type="text" name="cxs" id="cxs" required placeholder="col-xs-" value="${ combined.xs}">
				<label for="cxs">
					col-xs-
				</label>
			</div>
			<div class="twe-form__row">
				<input type="text" name="csm" id="csm" required placeholder="col-sm-" value="${ combined.sm}">
				<label for="csm">
					col-sm-
				</label>
			</div>
			<div class="twe-form__row">
				<input type="text" name="cmd" id="cmd" required placeholder="col-md-" value="${ combined.md}">
				<label for="cmd">
					col-md-
				</label>
			</div>

			<div class="twe-form__row">
				<input type="text" name="clg" id="clg" required placeholder="col-lg-" value="${ combined.lg}">
				<label for="clg">
					col-lg-
				</label>
			</div>


			<div class="twe-form__row">
				<input type="text" name="cxl" id="cxl" required placeholder="col-xl-" value="${ combined.xl}">
				<label for="cxl">
					col-xl-
				</label>
			</div>
		</div>

		<div class="twe-form__col--6">
			<div class="twe-form__row">
				<input type="text" name="cxso" id="cxso" required placeholder="col-xs-offset" value="${ combined.xso}">
				<label for="cxso">
					col-xs-offset
				</label>
			</div>
			<div class="twe-form__row">
				<input type="text" name="csmo" id="csmo" required placeholder="col-sm-offset" value="${ combined.smo}">
				<label for="csmo">
					col-sm-offset
				</label>
			</div>
			<div class="twe-form__row">
				<input type="text" name="cmdo" id="cmdo" required placeholder="col-md-offset" value="${ combined.mdo}">
				<label for="cmdo">
					col-md-offset
				</label>
			</div>

			<div class="twe-form__row">
				<input type="text" name="clgo" id="clgo" required placeholder="col-lg-offset" value="${ combined.lgo}">
				<label for="clgo">
					col-lg-offset
				</label>
			</div>


			<div class="twe-form__row">
				<input type="text" name="cxlo" id="cxlo" required placeholder="col-xl-offset" value="${ combined.xlo}">
				<label for="cxlo">
					col-xl-offset
				</label>
			</div>
		</div>

	</form>`;
};