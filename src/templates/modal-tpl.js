module.exports = ({ data, partial }) => {
    // console.log('modal tpl data', data);
    console.log('modal partial: ', partial);
    return `
        <div class="twe-modal">
            <div class="twe-modal__container container">
                <div class="twe-modal__header row">
                    <div class="col-xs-12"><h6>Hello</h6></div>
                </div>
                <div class="twe-modal__content row">
                    <div class="col-xs-12">
                        ${partial({ data })}
                    </div>
                </div>
                <div class="twe-modal__footer row">
                    <div class="col-xs-12">
                        <button class="twe-btn twe-btn--default js-save"><span>Save</span></button>
                        <button class="twe-btn twe-btn--cancel js-cancel"><span>Cancel</span></button>
                    </div>
                </div>
            </div>
        </div>`
};