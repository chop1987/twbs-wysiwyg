const webpack = require('webpack');

const path = require('path');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const libName = 'twbs-wysiwyg';

module.exports = {
  entry: './src/index.js',
  devtool: 'source-map',
  output: {
    filename: libName + '.js',
    path: path.resolve(__dirname, 'dist'),
    library: libName,
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: './node_modules/tinymce/skins', to: './skins' }
    ]),
    new MiniCssExtractPlugin({
      filename: "css/twbs-wysiwyg-style.css"
    })
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      }
    ]
  }
};
