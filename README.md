# My project's README



##Editor
.twe-content is the wrapper in which all the tinyMCE html code will be.
This will be hidden (or whatever) in the editor


##formCreator

var fields = [
    {
        type : 'text',
        label : 'first name',
        name : 'fname',
        description : 'bladibla bla',
        value : 'derp'
    },
    {
        type : 'select',
        label: 'XS width',
        name: 'xswidth',
        description: 'what care do you have?',
        options: [
            {
                text: 'col-xs-1',
                value: 'col-xs-1'
            },
            {
                text: 'col-xs-2',
                value: 'col-xs-2'
            },
            {
                text: 'col-xs-3',
                value: 'col-xs-3'
            },
            {
                text: 'col-xs-4',
                value: 'col-xs-4'
            },
            {
                text: 'col-xs-5',
                value: 'col-xs-5'
            },
            {
                text: 'col-xs-6',
                value: 'col-xs-6'
            },
            {
                text: 'col-xs-7',
                value: 'col-xs-7'
            },
            {
                text: 'col-xs-8',
                value: 'col-xs-8'
            },
            {
                text: 'col-xs-9',
                value: 'col-xs-9'
            },
            {
                text: 'col-xs-10',
                value: 'col-xs-10'
            },
            {
                text: 'col-xs-11',
                value: 'col-xs-11'
            },
            {
                text: 'col-xs-12',
                value: 'col-xs-12'
            }

        ]

    }
];

var formObj = {
    title : 'title',
    name: 'form1',
    fields: fields
}
var modal = formCreator.create(formObj);


##Tips
- the demo needs to be run from a web server, else it will give cross origin.   
an easy way to do that is:   
$ python -m SimpleHTTPServer 8080   
- to stop the server: $ kill